// ==UserScript==
// @name         DiscordSearchStat
// @namespace    https://gitlab.com/An0/DiscordHacks
// @version      1.0
// @description  Create stats from searches
// @author       An0
// @license      LGPLv3 - https://www.gnu.org/licenses/lgpl-3.0.txt
// @downloadURL  https://gitlab.com/An0/DiscordHacks/raw/master/SearchStat.user.js
// @match        https://*.discord.com/channels/*
// @match        https://*.discord.com/activity
// @match        https://*.discord.com/login*
// @match        https://*.discord.com/app
// @match        https://*.discord.com/library
// @match        https://*.discord.com/store
// @grant        unsafeWindow
// ==/UserScript==


(function() {

'use strict';

const BaseColor = "#fc0";

var Discord;
var Utils = {
    Log: (message) => { console.log(`%c[SearchStat] %c${message}`, `color:${BaseColor};font-weight:bold`, "") },
    Warn: (message) => { console.warn(`%c[SearchStat] %c${message}`, `color:${BaseColor};font-weight:bold`, "") },
    Error: (message) => { console.error(`%c[SearchStat] %c${message}`, `color:${BaseColor};font-weight:bold`, "") }
};

const weekDays = [ 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun' ];
const hoursHeader = '   ' + Array.from({length: 24}, (_, i) => i.toString().padStart(3)).join("");

var dispatchHook;
function Init(nonInvasive)
{
    Discord = { window: (typeof(unsafeWindow) !== 'undefined') ? unsafeWindow : window };

    if(Discord.window.webpackJsonp == null) { if(!nonInvasive) Utils.Error("Webpack not found."); return 0; }

    const webpackExports = typeof(Discord.window.webpackJsonp) === 'function' ?
          Discord.window.webpackJsonp(
              [],
              { '__extra_id__': (module, _export_, req) => { _export_.default = req } },
              [ '__extra_id__' ]
          ).default :
          Discord.window.webpackJsonp.push( [
              [],
              { '__extra_id__': (_module_, exports, req) => { _module_.exports = req } },
              [ [ '__extra_id__' ] ] ]
          );

    delete webpackExports.m['__extra_id__'];
    delete webpackExports.c['__extra_id__'];

    const findModule = (filter, nonInvasive) => {
        for(let i in webpackExports.c) {
            if(webpackExports.c.hasOwnProperty(i)) {
                let m = webpackExports.c[i].exports;

                if(!m) continue;

                if(m.__esModule && m.default) m = m.default;

                if(filter(m)) return m;
            }
        }

        if (!nonInvasive) {
            console.warn("Couldn't find module in existing cache. Loading all modules.");

            for (let i = 0; i < webpackExports.m.length; i++) {
                try {
                    let m = webpackExports(i);

                    if(!m) continue;

                    if(m.__esModule && m.default) m = m.default;

                    if(filter(m)) return m;
                }
                catch (e) { }
            }

            console.warn("Cannot find module.");
        }

        return null;
    };

    const findModuleByUniqueProperties = (propNames, nonInvasive) => findModule(module => propNames.every(prop => module[prop] !== undefined), nonInvasive);

	let dispatchModule = findModuleByUniqueProperties([ 'dispatch', 'maybeDispatch', 'dirtyDispatch' ], nonInvasive);
	if(dispatchModule  == null) { if(!nonInvasive) Utils.Error("dispatchModule not found."); return 0; }

	dispatchHook = Discord.original_dispatch = dispatchModule.dispatch;
    dispatchModule.dispatch = function() { return dispatchHook.apply(this, arguments); };

	let timestampSet;
	let timestampSetSize;
	let bulkFilterSet;
	let bulkFilterSetSize;
	let daymap;
	let timezoneOffset;
	let lastFilterValue;
	Discord.window.StartSearchStat = (timezoneHour, bulkFilterValue) => {
		timestampSet = new Set();
		bulkFilterSet = new Set();
	    timestampSetSize = 0;
		bulkFilterSetSize = 0;
		timezoneOffset = isFinite(timezoneHour) ? timezoneHour * 60*60*1000 : new Date().getTimezoneOffset() * -60000;
		bulkFilterValue = (bulkFilterValue || 600) * 1000;
		if(bulkFilterValue === 0) bulkFilterValue = 1;
		lastFilterValue = 0;
		daymap = Array.from({length: 7}, () => new Array(24).fill(0));
		
		dispatchHook = function(event) {
			if(event.type === 'SEARCH_FINISH') {
				let warnDuplicates = false;
				
				for(const group of event.messages) {
					const message = (group.length === 5) ? group[2] : group.find(x => x.hit);
					const timestamp = Number(BigInt(message.id) >> 22n) + 14200704e5/*DISCORD_EPOCH*/;
					
					timestampSet.add(timestamp);
					if(timestampSet.size !== timestampSetSize) {
						timestampSetSize++;
						
						let filterValue = Math.floor(timestamp / bulkFilterValue);
						if(lastFilterValue !== filterValue) {
							lastFilterValue = filterValue;
							bulkFilterSet.add(filterValue);
							if(bulkFilterSet.size !== bulkFilterSetSize) {
								bulkFilterSetSize++;
								const date = new Date(timestamp + timezoneOffset);
								daymap[date.getUTCDay()][date.getUTCHours()]++;
							}
						}
						
					}
					else warnDuplicates = true;
				}
				
				if(warnDuplicates)
					Utils.Warn("You are hitting duplicate results");
				
				let fixedDaymap = [...daymap];
				fixedDaymap.push(fixedDaymap.shift());
				
				let maxConcentration = Math.max(...fixedDaymap.map(x => Math.max(...x)));
				if(maxConcentration !== 0) {
					let params = [];
					let currentValue = 0;
					let output = hoursHeader;
					const addData = (data, value) => {
						if(currentValue !== value) {
							output += '%c' + data;
							params.push(`background:rgba(53,28,117,${value/maxConcentration})`);
							currentValue = value;
						}
						else output += data;
					}
					fixedDaymap.forEach((dayHours, i) => {
						addData(`\n${weekDays[i]}`, 0);
						dayHours.forEach((hourCount, i) => {
							addData("   ", hourCount);
						});
					});
					console.log(output, ...params);
				}
			}
			return Discord.original_dispatch.apply(this, arguments);
		};
		
		return 'OK';
	};
	
	Discord.window.EndSearchStat = () => {
		dispatchHook = Discord.original_dispatch;
		timestampSet = null;
		bulkFilterSet = null;
		daymap = null;
		
		return 'OK';
	};


    Utils.Log("loaded");

    return 1;
}


var InitFails = 0;
function TryInit()
{
    if(Init(true) !== 0) return;

    window.setTimeout((++InitFails === 600) ? Init : TryInit, 100);
};


TryInit();

})();
