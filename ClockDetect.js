//Just paste this into the console, it creates a pseudo ping that's the difference of their clock and the server's time plus their ping time
//Messages sent from android will be way off sadly

const webpackExports = typeof(window.webpackJsonp) === 'function' ?
    window.webpackJsonp(
        [],
        { '__extra_id__': (module, _export_, req) => { _export_.default = req } },
        [ '__extra_id__' ]
    ).default :
    window.webpackJsonp.push( [
        [],
        { '__extra_id__': (_module_, exports, req) => { _module_.exports = req } },
        [ [ '__extra_id__' ] ] ]
    );

delete webpackExports.m['__extra_id__'];
delete webpackExports.c['__extra_id__'];

const findModule = (filter) => {
    for(let i in webpackExports.c) {
        if(webpackExports.c.hasOwnProperty(i)) {
            let m = webpackExports.c[i].exports;
            if(!m) continue;
            if(m.__esModule && m.default) m = m.default;
            if(filter(m)) return m;
        }
    }
    return null;
};

const findModuleByUniquePrototype = (propNames) => findModule(module => module.prototype && propNames.every(prop => module.prototype[prop] !== undefined));
const WebsocketReceiver = findModuleByUniquePrototype(['emit', '_handleDispatch']);

const OLDEMIT = WebsocketReceiver.prototype.emit;
WebsocketReceiver.prototype.emit = function(_, type, message) {
    if(type === "MESSAGE_CREATE" && /^\d+$/.test(message.nonce) && !/^[⠀-⣿]{16}/.test(message.content) && (message.embeds == null || message.embeds.length == 0 || message.embeds[0].author == null || !message.embeds[0].author.name.startsWith('-----'))) {
        try {
            message.embeds.push({description:((BigInt(message.id) >> 22n) - (BigInt(message.nonce) >> 22n)).toString()});
        } catch(e) { console.log(e) }
    }
    return OLDEMIT.apply(this, arguments);
};