############################################
# Don't get logged out when discord closes #
############################################

$ErrorActionPreference = "Stop"

function AddExtension([string]$electonDataPath) {
	'adding extension'
	$extensionListPath = "$electonDataPath\DevTools Extensions"
	if(Test-Path $extensionListPath) {
		$extensionList = ConvertFrom-Json (Get-Content $extensionListPath -Raw)
		$extensionList = @($extensionList | ? { $_ -notmatch '(?:^|[\\\/])DiscordNoLogout[\\\/]?$' })
		if($extensionList.Length -ne 0) {
			$extensionList += '../../DiscordNoLogout'
			Set-Content $extensionListPath (ConvertTo-Json $extensionList)
			return
		}
	}
	Set-Content $extensionListPath '["../../DiscordNoLogout"]'
}

function StopProcesses([string]$name) {
	$targets = Get-Process $name -ErrorAction SilentlyContinue
	if($targets.Length -eq 0) { return }
	try {
		$targets | Stop-Process
	} catch { "PLEASE CLOSE $name!" }
	"waiting for $name to close"
	do {
		sleep 1
		$targets = Get-Process $name -ErrorAction SilentlyContinue
	} while($targets.Length -gt 0)
}

$discordPath = $env:LOCALAPPDATA+'\Discord'
$discordDataPath = $env:APPDATA+'\discord'
$discordResourcesPath = $discordPath+'\app-*\resources'
$discordPtbPath = $env:LOCALAPPDATA+'\DiscordPTB'
$discordPtbDataPath = $env:APPDATA+'\discordptb'
$discordPtbResourcesPath = $discordPtbPath+'\app-*\resources'
$discordCanaryPath = $env:LOCALAPPDATA+'\DiscordCanary'
$discordCanaryDataPath = $env:APPDATA+'\discordcanary'
$discordCanaryResourcesPath = $discordCanaryPath+'\app-*\resources'
$pluginPath = $env:LOCALAPPDATA+'\DiscordNoLogout'


$install = $false

try {

if(Test-Path $discordPath) {
	'Discord found'
	if(Test-Path $discordDataPath) { 'data directory found' } else { 'data directory not found'; return }
	if(Test-Path $discordResourcesPath) { 'resources directory found' } else { 'resources directory not found'; return }
	
	StopProcesses 'Discord'

	AddExtension $discordDataPath

	$install = $true
}

if(Test-Path $discordPtbPath) {
	'DiscordPTB found'
	if(Test-Path $discordPtbDataPath) { 'data directory found' } else { 'data directory not found'; return }
	if(Test-Path $discordPtbResourcesPath) { 'resources directory found' } else { 'resources directory not found'; return }
	
	StopProcesses 'DiscordPTB'

	AddExtension $discordPtbDataPath

	$install = $true
}

if(Test-Path $discordCanaryPath) {
	'DiscordCanary found'
	if(Test-Path $discordCanaryDataPath) { 'data directory found' } else { 'data directory not found'; return }
	if(Test-Path $discordCanaryResourcesPath) { 'resources directory found' } else { 'resources directory not found'; return }
	
	StopProcesses 'DiscordCanary'
	
	AddExtension $discordCanaryDataPath

	$install = $true
}


if($install) {
	'installing'

	[void](New-Item "$pluginPath\manifest.json" -Type File -Force -Value @'
{
	"name": "DiscordNoLogout",
	"content_scripts": [ {
		"js": [ "DiscordNoLogoutLoader.js" ],
		"matches": [ "*" ],
		"run_at": "document_start"
	} ]
}
'@)

	[void](New-Item "$pluginPath\DiscordNoLogoutLoader.js" -Type File -Force -Value @'
// https://gitlab.com/An0/DiscordHacks

let script = document.createElement('script');
script.textContent = `

localStorage.removeItem = function(key) { if(key !== 'token') delete this[key]; };

`;
(document.head||document.documentElement).appendChild(script);
script.remove();
'@)

	'FINISHED'
}
else { 'Discord not found' }

}
catch { $_ }
finally { [Console]::ReadLine() }
